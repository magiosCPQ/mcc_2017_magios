number = raw_input('Please enter a number? ')
num = int(number)

# Check if multiple of 4
if num % 4 == 0:
    print number + " is multiple of 4"

# Check Prime
if num > 1:
    for i in range(2, num):
        if (num % i) == 0:
            print num, "is not a prime number"
            break #if there is no break, the else catches the end of the loop
    else:
        print num, "is a prime number"
# if input number is less than
# or equal to 1, it is not prime
else:
    print num, "is not a prime number"

# Check even or odd by a bit AND with the first bit
if int(number) & 1 == 0:
    print number + " is even"
else:
    print number + " is odd"

# Test if check divides num exactly
number = raw_input('Please enter num ')
check = raw_input('Please enter check ')

if int(number) % int(check) == 0:
    print number + " is divisible by " + check
else:
    print number + " is not divisible by " + check