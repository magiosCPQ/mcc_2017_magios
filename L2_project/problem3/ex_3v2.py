#!/usr/bin/env python
word = raw_input("Please enter a string: ")
word = word.lower()
print ("It is a palindrome") if word[::-1] == word else ("Not a palindrome")

'''
if word[::-1] == word:
    print ("It is a palindrome")
else:
    print ("Not a palindrome")
'''
