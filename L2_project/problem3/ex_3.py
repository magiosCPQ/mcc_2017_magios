#!/usr/bin/env python
import unittest
def palindrome(word=''):
    word = word.lower()
    if word[::-1] == word:
        print ("It is a palindrome")
        return True
    else:
        print ("Not a palindrome")
        return False

def palindrome2(n=''):
    if str(n) == str(n)[::-1]:
        return True
    else:
        return False

class TestStringMethods(unittest.TestCase):
    """Right- Happy path
       B    - Boundary
       I    - Inverse
       C    - Cross-check
       E    - Error
       P    - Performance
       """
    def test_Boundary_Upper(self):
        self.assertTrue(palindrome('oofOo'))


    def test_Bundary_number(self):
        self.assertTrue(palindrome('123321'))

    def test_Cross_check(self):
        self.assertEqual(palindrome2("abccba"),palindrome("abccba"))

    def test_Error(self):
        self.assertFalse(palindrome('123'))

def main():
    unittest.main()
if __name__ == '__main__':
    main()