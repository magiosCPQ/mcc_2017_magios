import random
import re
DEBUG = False
#DEBUG = True
NUMBER_OF_DIGITS = 4
p = re.compile("^" + "[0-9]"*NUMBER_OF_DIGITS + "$")   # Generates pattern to validate user input

# Generate list with N random numbers
TEST_NUM = [str(random.randint(0, 9)) for x in range(0, NUMBER_OF_DIGITS)]
if DEBUG: print TEST_NUM

user_input = ''
correct_guess = False
guess_count = 0
while user_input != 'skip' and not correct_guess:
    ## Parse user input
    user_input = raw_input("Make your guess ("+str(NUMBER_OF_DIGITS)+" digit number or 'skip' to exit): ").strip()
    # Validate input
    if not p.match(user_input):
        print "Error:",user_input,"is invalid. Please enter " + str(NUMBER_OF_DIGITS) + " numbers or 'skip'"
        continue

    ## Init check variables
    cows_count  = 0
    bulls_count = 0
    copy_test_num = TEST_NUM[:]
    user_input_list = list(user_input)

    ## Mark all cows first in the user input and the target number
    for i in range(0, NUMBER_OF_DIGITS):
        if copy_test_num[i] == user_input_list[i]:
            cows_count += 1
            copy_test_num[i] = 'v'
            user_input_list[i] = 't'

    ## Find the bulls and mark the one found to avoid repeating it
    for i in range(0, NUMBER_OF_DIGITS):
        if user_input_list[i] in copy_test_num:
            bulls_count += 1
            indexOfBull = copy_test_num.index(user_input_list[i])
            copy_test_num[indexOfBull] = 'b'

    ## Keep track of the attempts
    guess_count += 1

    ## If user nailed it
    if cows_count == NUMBER_OF_DIGITS:
        correct_guess = True
        print "Congraturations youre win!!"

    ## User failed, display cows and bulls count
    else:
        print "cows: "+ str(cows_count) + ", bulls: "+ str(bulls_count)

## User wins display attempts
if(correct_guess):
    print "It took you " + str(guess_count) + " guesses"
