import csv
import os
if 'DEBUG' in globals():
    DEBUG = globals()['DEBUG']
elif 'DEBUG' in locals():
    DEBUG = locals()['DEBUG']
else:
    DEBUG = False


def is_pair(num):
    """
    Check if a number is pair.
    Raises TypeError exception if the argument is not of type int or float.

    :param num: A float or int number
    :return: True if the number is pair, False if is odd
    """
    if not isinstance(num, (int, float)):
        raise TypeError("Argument must be int or float")

    return num & 1 == 0 if type(num) is int else num % 2 == 0


class StatisticAnalizer:
    def __init__(self, data_file='', data_set=[]):
        """
        Creates an instance and populates the data set from a file or a list
        of floats.

        :param data_file: path of the csv file to populate the data set
        :param data_set: list of floats to populate the data set
        """
        if data_file:
            abs_data_file = os.path.abspath(data_file)
            with open(abs_data_file,'rb') as f:
                reader = csv.reader(f)
                parsed_data = list(reader)

                if not len(parsed_data):
                    print "No data in the file"
                    self.Data_set = data_set
                else:
                    self.Data_set = map(float, parsed_data[0])
        else:
            self.Data_set = map(float, data_set)

    def population_mean(self, data=None):
        """
        Calculates the average value of the data given. By default uses the
        instance data set.

        :param data: list of float values
        :return: average value of the data
        """
        if not data:
            data = self.Data_set
        return sum(data) / float(len(data))

    def variance(self, data=None, data_mean=None):
        """
        Calculates the variance by formula. Can calculate it for a given data
        set. By default uses the instance data set.

        :param data: list of floats to calculate the variance
        :param data_mean: mean value of the data set given
        :return: variance of the data
        """
        if not data:
            data = self.Data_set
            data_mean = self.population_mean(data)
        if not data_mean:
            data_mean = self.population_mean(data)
        return sum([(x - data_mean) ** 2 for x in data])

    def covariance(self, x=None, x_=None, y=None, y_=None):
        """
        Calculates the covariance. By default uses the instance data set
        assuming that x goes from 0 to length of the data set

        :param x: list with the x values or range(len(y)) by default
        :param x_: mean value of the x list
        :param y: y values or the instance data set by default
        :param y_: mean value of the y list
        :return: Covariance as a float
        """
        if not y:
            y = self.Data_set
            y_ = self.population_mean(data=y)
        if not y_:
            y_ = self.population_mean(data=y)
        if not x:
            x = range(len(y))
            x_ = self.population_mean(data=x)
        if not x_:
            x_ = self.population_mean(data=x)

        covar = 0.0
        for i in range(len(x)):
            covar += (x[i] - x_) * (y[i] - y_)
        return covar

    def std_deviation(self):
        """
        Calculates the standard deviation by formula

        :return: Standard Deviation as float
        """
        n = len(self.Data_set)
        return (self.variance()/(n - 1))**0.5

    def mode(self):
        return max(set(self.Data_set), key=self.Data_set.count)

    def median(self, start=0, end=None):
        """
        Calculates the median of the data in the inclusive given indexes.
        Uses all data by default and might support negative indexes

        :param start: Inclusive data beginning index
        :param end: Inclusice data ending index
        :return: value corresponding to the median of the given indexes
        """
        if not end:
            end = len(self.Data_set) - 1
        end = end if end >= 0 else len(self.Data_set) + end
        start = start if start >= 0 else len(self.Data_set) + start

        # swap to make end the highest value
        if start > end :
            end, start = start, end

        size = end - start + 1
        median_index = start + size / 2
        if is_pair(size):
            return (self.Data_set[median_index] + self.Data_set[median_index-1]) / 2.0

        return self.Data_set[median_index]

    def quartiles(self):
        """
        This function use the method T1-83 to solve the quartiles.
        the media divides the population in 2 subgroups H1, H2

        :return: [Q1, Q2, Q3]
        * Q1 =  it is the media of H1 subgroup
        * Q2 =  the media of the enteria population.
        * Q3 =  it is the media of H2 subgroup
        """
        self.Data_set.sort()
        size = len(self.Data_set)
        half_size = size / 2
        if not is_pair(size):
            q1 = self.median(end=half_size-1)
            q2 = self.median()
            q3 = self.median(start=half_size+1, end=size-1)
        else:
            q1 = self.median(end=half_size-1)
            q2 = self.median()
            q3 = self.median(start=half_size)
        return [q1, q2, q3]

    def linear_regression(self):
        """
        Calculates the order 1 linear regression from the data_set and returns
        the coefficients of the equation, where the index is the power of the
        x.
        Example: [b0, b1] -> y = b0 + b1*x

        :return: list of coefficients where the index represents the power of x
        """
        x = range(len(self.Data_set))
        y = self.Data_set
        x_, y_ = self.population_mean(x), self.population_mean(y)
        b1 = self.covariance(x=x, x_=x_, y=y, y_=y_) / self.variance(data=x, data_mean=x_)
        b0 = y_ - b1 * x_
        return [b0, b1]


def main():
    # my_list = [6, 7, 15, 36, 39, 40, 41, 42, 43, 47, 49]
    # my_list = [7, 15, 36, 39, 40, 40, 41, 65]
    # my_list = [7, 15, 36, 39, 40, 41]
    my_list = [7, 15, 36, 39, 40, 40, 41, 65, 68, 69]
    #statistics = Operation(data_set=my_list)

    statistics = StatisticAnalizer(data_file='/home/devuser/magios/mcc_2017_magios/L4_project/problem9/data.csv')
    mean = statistics.population_mean()
    print ('Population Mean: ', mean)

    std_deviation = statistics.std_deviation()
    print ('Standard deviation: ', std_deviation)

    mode = statistics.mode()
    print ('Mode: ', mode)

    quartile = statistics.quartiles()
    print ('Quartiles: ', quartile)

    coefficients = statistics.linear_regression()
    print ('Linear Regression:', 'y=%s + (%s)*x' % (str(coefficients[0]), str(coefficients[1])))

if __name__ == '__main__':
    main()
