import unittest, bisect
#!/usr/bin/env python
"""
Write a Python program for binary search.

Binary Search:
    In computer science, a binary search or half-interval search algorithm
    finds the position of a target value within a sorted array. The binary
    search algorithm can be classified as a dichotomies divide-and-conquer
    search algorithm and executes in logarithmic time.

Test Data:
    binary_search([1,2,3,5,8], 6) -> False
    binary_search([1,2,3,5,8], 5) -> True
"""


def binary_search(lst, elem):
    """
    Search the position of an element in an ordered list of comparable objects
    and returns the position on the list where it was found, or None if not
    found

    :param lst: An ordered (low->high) list of comparable objects
    :param elem: Target element to find in the list
    :return: Position in the list if found, or None if not found
    """
    left = 0
    right = len(lst) - 1

    while left <= right:
        mid = (left + right)/2
        if lst[mid] < elem:
            left = mid + 1
        elif lst[mid] > elem:
            right = mid - 1
        else:
            return mid

    return None

def index(a, x):
    'Locate the leftmost value exactly equal to x'
    i = bisect.bisect_left(a, x)
    if i != len(a) and a[i] == x:
        return i
    return None


class MyTest(unittest.TestCase):
    def test_middle_search(self):
        lst = [1, 2, 3, 5, 8]
        x = lst.index(3)
        self.assertEqual(binary_search(lst,3), x)

    def test_end_search(self):
        lst = [1, 2, 3, 5, 8]
        x = lst.index(8)
        self.assertEqual(binary_search(lst,8), x)

    def test_start_search(self):
        lst = [1, 2, 3, 5, 8]
        x = lst.index(1)
        self.assertEqual(binary_search(lst, 1), x)

    def test_no_val_search(self):
        lst = [1, 2, 3, 5, 8]
        self.assertEqual(binary_search(lst,10), None)

    def test_repeated_vals_together_search(self):
        lst = [2, 2, 3, 5, 8]
        x = index(lst, 2)
        self.assertEqual(binary_search(lst,2), x)

    def test_repeated_vals_start_end_search(self):
        lst = [2, 4, 3, 5, 2]
        x = index(lst, 2)
        self.assertEqual(binary_search(lst,2), x)


def main():
    """
    Function called when running as standalone script. Used for testing functionality.

    :return: None
    """
    unittest.main()
    '''
    lst = [1, 2, 3, 5, 8]
    print "Test Case 1"
    print "binary_search([1,2,3,5,8], 6) ->", str(binary_search(lst, 6))
    print
    print "Test Case 2"
    print "binary_search([1,2,3,5,8], 5) ->", str(binary_search(lst, 5))
    '''

if __name__ == '__main__':
    main()
