#!/usr/bin/env python
"""
Few functions to generate a square matrix and calculate the determinant.
When the DEBUG flag is set to True it will show additional information at
run time.
"""
if 'DEBUG' in globals():
    DEBUG = globals()['DEBUG']
elif 'DEBUG' in locals():
    DEBUG = locals()['DEBUG']
else:
    DEBUG = False


def str2matrix(str_matrix):
    """
    Converts a matrix string matlab style into a 2-dimensional list

    :param str_matrix: A string describing the matrix rows where elements are separated by ' ' and rows are separated by';'. As "[1 2 3; 4 5 6; 7 8 9]"
    :return: 2-dimensional list representing a square matrix. Returns None if an error was detected.
    """
    str_matrix = str_matrix.strip("[]")
    # Generate the matrix of floats from the ';' separated string
    mat = [map(float, row.strip().split()) for row in str_matrix.split(';')]

    # Validate the matrix dimensions
    n_rows = len(mat)
    for row in mat:
        if len(row) != n_rows:
            print "Error: Matrix:\n%sis not a square matrix" % print_matrix(mat)
            return None

    print "\nCreated Matrix:"
    print print_matrix(mat)
    return mat


def print_matrix(mat):
    """
    Receives a 2-dimensional list and returns a string separating each row by
    a new line.

    :param mat: A 2-dimensional list representing a matrix
    :return: A string with each matrix row separated by new line characters
    """
    str_matrix = ''
    for row in mat:
        str_matrix += str(row) + '\n'
    return str_matrix


def det(mat):
    """
    Calculates the determinant of square matrices of order 1 to 3

    :param mat: A 2-dimensional list representing a square matrix
    :return: Result of the determinant as *float* or `None` if an error is detected
    """
    scalar = 0.0
    order = len(mat)

    # Determinant of a 1x1 matrix is the element in the matrix
    if order == 1:
        return mat[0][0]
    if order > 3:
        print "ERROR: Imposibru to calculate that for now!"
        return None

    # Store the product of left to right diagonal and right to left diagonal to sum them at the end
    for row_index in range(order):
        # If 2x2 matrix calculate diagonals only once
        if order == 2 and row_index > 0:
            return scalar

        diagonals = [1.0, 1.0]          # init the products
        for col_index in range(order):
            tmp_index = row_index+col_index
            left_to_right_index = tmp_index if tmp_index < order else tmp_index - order

            tmp_index = row_index + (order - 1) - col_index
            right_to_left_index = tmp_index if tmp_index < order else tmp_index - order

            if DEBUG:
                print (left_to_right_index, right_to_left_index)
                print (mat[col_index][left_to_right_index], mat[col_index][right_to_left_index])

            diagonals[0] *= mat[col_index][left_to_right_index]
            diagonals[1] *= mat[col_index][right_to_left_index]

        scalar += diagonals[0] - diagonals[1]

    return scalar


def main():
    """
    Function called when running as standalone script. Used for testing functionality.

    :return: None
    """
    str_matrix_3x3 = "[1 2 3; 0 -4 1; 0 3 -1]"
    str_matrix_2x2 = "[1 2; 3 4]"
    str_matrix_1x1 = "[5]"

    str_matrix_list = [str_matrix_1x1, str_matrix_2x2, str_matrix_3x3]

    for str_matrix in str_matrix_list:
        print
        print '='*60
        print "Matrix string given:", str_matrix
        mat = str2matrix(str_matrix)
        if not mat:
            print "Fail!"
            exit(-1)

        print "Determinant = " + str(det(mat))


if __name__ == "__main__":
    main()
