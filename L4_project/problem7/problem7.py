#!/usr/bin/env python
import unittest
"""
Write a program (function!) that takes a list and returns a new list that
contains all the elements of the first list minus all the duplicates.

Extras:
    * Write two different functions to do this - one using a loop and
      constructing a list, and another using sets.
"""


def list_minus(l1, l2):
    """
    Receives 2 lists. Removes elements in list 1 that exist in list 2. Using
    loop approach
    :param l1: List with hashable type elements like string, int, float, etc.
    :param l2: List with hashable type elements like string, int, float, etc.
    :return: New list with elements in l1 that are not contained in l2
    """
    return [elem for elem in l1 if elem not in l2]


def set_minus(l1, l2):
    """
    Receives 2 lists. Removes elements in list 1 that exist in list 2. Using
    set minus operation
    :param l1: List with hashable type elements like string, int, float, etc.
    :param l2: List with hashable type elements like string, int, float, etc.
    :return: New list with elements in l1 that are not contained in l2
    """
    return list(set(l1) - set(l2))


class Testminus(unittest.TestCase):

    def test_minus(self):
        l1 = [1, 2, 3, 'one', 'two', 'three']
        l2 = [1, 3, 'one', 'three']
        l3=[2,"two"]
        self.assertEqual(list_minus(l1, l2), list(set(l1) - set(l2)))
        self.assertEqual(list_minus(l1, l2), list(l3))

    def test_minus2(self):
        l1 = [0, 2, 'zero']
        l2 = [0,'zero',1]
        lmalo=[2,0,0]
        self.assertFalse(list_minus(l1, l2)==list(lmalo))

    def test_minus3(self):
        l1 = ['pepe','hola','adios']
        l2 = ['hola', 'adios']
        lbien=['pepe']
        self.assertTrue(list_minus(l1, l2)==list(lbien))


if __name__ == '__main__':
    unittest.main()
